# -*- coding: utf-8 -*-
##
#RedClara
##

from datetime import datetime
from itertools import islice
from MaKaC.services.implementation.base import ProtectedModificationService, ParameterManager
from MaKaC.services.implementation.base import ProtectedDisplayService
from MaKaC.services.implementation.base import TextModificationBase
from MaKaC.services.implementation.base import ExportToICalBase
from MaKaC.services.implementation.base import LoggedOnlyService

import MaKaC.conference as conference
from MaKaC.services.interface.rpc.common import ServiceError, ServiceAccessError
import MaKaC.webinterface.locators as locators
from MaKaC.webinterface.wcomponents import WConferenceListItem
from MaKaC.common.fossilize import fossilize
from MaKaC.user import PrincipalHolder, Avatar, Group, AvatarHolder
from indico.core.index import Catalog
from indico.web.http_api.util import generate_public_auth_request
import MaKaC.common.info as info
from MaKaC import domain
from MaKaC.common.Configuration import Config
from MaKaC.webinterface.mail import GenericMailer, GenericNotification
import MaKaC.webinterface.urlHandlers as urlHandlers
#from MaKaC.webinterface.urlHandlers import UHCategModifAC

from MaKaC.webinterface.rh.base import RH, RHProtected

from collections import OrderedDict
from xml.sax.saxutils import unescape, escape
import time
import pprint

class GetEventsList(RH):

    def _checkParams(self, params):
        
        self._params = params
        
        
    def initialize(self):
        self.filterCategoryTags(self._params)
        self._lastIdx = int(self._params.get("lastIdx", 0))
        
        categoryId                 = int(self._params["categId"])
        l                          = locators.CategoryWebLocator( {"categId": str(categoryId), "others": ""}, 1 )
        self._target = self._categ = l.getObject()
        self._events = {}
        self._recentEvents = {}
        
        
    def checkRequest(self):
        
        if "api_key" in self._params:
            if not self._params["api_key"] == "693b2d384d2c395331243253662b5f5d6c594d2c733b7227333f79505c":
                return "<msg>Invalid api_key</msg>"
        else:
            return "<msg>The api_key is required</msg>"
        
        if not "categId" in self._params:
            self._params["categId"] = 0
        
        try:
            self._categoryId = int(self._params["categId"])
        except ValueError,e:
            return "<msg>The categId is not valid integer.</msg>"
        
        return False
        
        
    def _process(self):
        
        isNotValidRequest = self.checkRequest()
        if isNotValidRequest:
            return self._generateXMLError(isNotValidRequest)
        
        self.initialize()
        
        return self._getAnswer()
        
    def getEvents(self, category):
        if category.hasSubcategories():
            subCats = category.getSubCategoryList()
            
            for cat in subCats:
                self.getEvents(cat)                
        
        index      = Catalog.getIdx('categ_conf_sd').getCategory(category.getId())
        skip       = max(0, self._lastIdx - 100)
        pastEvents = {}
        num        = 0
        
        for event in index.itervalues():
            
            #Inicializacion de variables:
            #raise Exception(dir(event))
            if 'communityTag' in self._params:
                if (self._params['communityTag'] != "") and (event.getCommunityTag() != self._params['communityTag']):
                    continue
                    
            sd     = event.getStartDate()
            ed     = event.getEndDate()
            _event = {
                    'title' : escape(event.getTitle()),
                    'startDate' : datetime(sd.year, sd.month, sd.day, 1).strftime("%Y-%m-%d"),
                    'startTime' : event.getStartDate().strftime("%H:%M"),
                    'endDate' : datetime(ed.year, ed.month, ed.day, 1).strftime("%Y-%m-%d"),
                    'endTime' : event.getEndDate().strftime("%H:%M"),
                    'timezone' : 'UTC',
                    'shortDate': datetime(sd.year, sd.month, 1).strftime("%B %Y"),
                    'categoryName': escape(category.getTitle()),
                    'categoryId': category.getId(),
                    'description' : escape(event.getDescription()),
                    'contactInfo': escape(event.getContactInfo()),
                    'url' : escape("""%s"""%urlHandlers.UHConferenceDisplay.getURL(event)),
                    'year': sd.year,
                    'month': sd.month,
                    'location': '',
                    'address' : '',
                    'communityTag' : escape(event.getCommunityTag()), 
                    'userEmail': False
                }
            
            if event.getLocation() != None:
                _event['location'] = escape(event.getLocation().getName())
                _event['address'] = escape(event.getLocation().getAddress())
                
            if self._params.get('userEmail', False):
                email = str(self._params.get('userEmail'))                
                if (event.getCreator() != None) and (event.getCreator().getEmail().lower() == email.lower()):
                    _event['userEmail'] = escape(email)
                    
                elif event.hasRegistrantByEmail(email):
                    _event['userEmail'] = escape(email)
            
            if event.getUnixStartDate() >= time.time():
                self._recentEvents[event.getId()] = _event
            else:
                self._events[event.getId()] = _event        
        

    def _getAnswer( self ):
        
        if self._target is None:
            return self._generateXMLError("""<msg>Category with id %s not found</msg>"""%self._params['categId'])
        
        self.getEvents(self._categ)
        
        #Ordenamiento de los eventos
        events = []
        if len(self._recentEvents) >= 5:
            events = sorted(self._recentEvents.items(), key=lambda t: t[1]['startDate'], reverse=True)[:5]
        
        if len(self._recentEvents) > 0:
            items  = len(self._recentEvents)
            newest = sorted(self._recentEvents.items(), key=lambda t: t[1]['startDate'], reverse=False)
            
            if(len(newest) < 5):
                oldest = sorted(self._events.items(), key=lambda t: t[1]['startDate'], reverse=True)[:5 - len(newest)]                
                for item in oldest:
                    newest.append(item)
                    
            newest = sorted(newest, key=lambda t: t[1]['startDate'], reverse=False)[:5]
            events = sorted(newest, key=lambda t: t[1]['startDate'], reverse=True)[:5]
        else:            
            events = sorted(self._events.items(), key=lambda t: t[1]['startDate'], reverse=True)[:5]
        
        #Si el parametro userEmail es referenciado, se ordenan para que aparescan primero los que tienen el email.
        if self._params.get('userEmail', False) and len(events) > 0:
            events = sorted(events, key=lambda t: t[1]['startDate'], reverse=False)
            return self._xmlSuccessResponse(sorted(events, key=lambda t: t[1]['userEmail'], reverse=True))
        
        return self._xmlSuccessResponse(sorted(events, key=lambda t: t[1]['startDate'], reverse=False))
    
    def _xmlSuccessResponse(self, events):
        xml = """
        <?xml version="1.0" encoding="utf-8"?>
            <data>
                <cmdStatus>SUCCESS</cmdStatus>
                <events>
        """
        
        for identifier, event in events:
            xml += "<event>"
            for key, value in event.items():
                xml += """<%s>%s</%s>"""%(str(key), str(value), str(key))
            
            xml += "</event>"
            
        xml += """
            </events>
        </data>
        """
        return xml
    
    def _generateXMLError(self, tag):
        return """
        <?xml version="1.0" encoding="UTF-8"?>
            <data>
                <cmdStatus>FAILED</cmdStatus>
                %s
            </data>
        """%tag
    
    def filterCategoryTags(self, params):
        
        tags = {
            '1'  : 8,
            '2'  : 19,
            '3'  : 6,
            '4'  : 20,
            '5'  : 9,
            '6'  : 7,
            '7'  : 21,
            '100': 10,
            '200': 29,
            '300': 1
        }
        
        self._params["categId"] = tags.get(str(self._params["categId"]), self._params["categId"])
        
# def getTimestampt(self, datetime, timezone):
#        if timezone == None:
##            timezone = "UTC"
#            
#        c.setTimezone(confData.get("Timezone", "UTC"))
#        tz = confData.get("Timezone", "UTC")
    