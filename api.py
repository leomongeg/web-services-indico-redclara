# -*- coding: utf-8 -*-
##
#RedClara
##

from datetime import timedelta, datetime

#################################
# Fermi timezone awareness      #
#################################
from pytz import timezone
from MaKaC.common.timezoneUtils import nowutc, DisplayTZ
#################################
# Fermi timezone awareness(end) #
#################################
import MaKaC.webinterface.rh.base as base
from MaKaC.webinterface.rh.base import RoomBookingDBMixin
import MaKaC.webinterface.locators as locators
import MaKaC.webinterface.wcalendar as wcalendar
import MaKaC.webinterface.webFactoryRegistry as webFactoryRegistry
import MaKaC.webinterface.urlHandlers as urlHandlers
import MaKaC.webinterface.pages.category as category
import MaKaC.webinterface.displayMgr as displayMgr
from MaKaC.errors import MaKaCError,FormValuesError,NoReportError
import MaKaC.conference as conference
from MaKaC.conference import ConferenceChair
from MaKaC.common.general import *
import MaKaC.statistics as statistics
from MaKaC.common.Configuration import Config
import MaKaC.user as user
import MaKaC.common.info as info
from MaKaC.i18n import _
from MaKaC.webinterface.user import UserListModificationBase
from MaKaC.common.utils import validMail, setValidEmailSeparators
from MaKaC.common.mail import GenericMailer
from MaKaC.webinterface.common.tools import escape_html
from indico.web.http_api.api import CategoryEventHook
from indico.web.http_api.metadata.serializer import Serializer
from indico.web.wsgi import webinterface_handler_config as apache
from MaKaC.webinterface.common.tools import cleanHTMLHeaderFilename


####PARA USUARIOS

import MaKaC.webinterface.rh.base as base
import MaKaC.webinterface.rh.admins as admins
import MaKaC.webinterface.pages.admins as adminPages
import MaKaC.user as user
import MaKaC.common.info as info
import MaKaC.errors as errors
import MaKaC.webinterface.urlHandlers as urlHandlers
import MaKaC.webinterface.mail as mail
from MaKaC.errors import MaKaCError, NotFoundError
from MaKaC.accessControl import AdminList
from MaKaC.webinterface.rh.base import RH, RHProtected
from MaKaC.authentication import AuthenticatorMgr
from MaKaC.common import DBMgr
from MaKaC.common import pendingQueues
import re
from MaKaC.i18n import _
import xmltodict
from xml.sax.saxutils import unescape, escape
import random
import pprint

class test(RH):
    _uh = urlHandlers.UHConferencePerformCreation
    def _checkParams( self, params ):
        self._params = params
            
    def initialize( self ):
        self._save                 = self._confData.get("Save", "")
        categoryId                 = int(self._confData["categId"])
        l                          = locators.CategoryWebLocator( {"categId": str(categoryId), "others": ""}, 1 )
        self._target               = l.getObject()
        self._wf                   = None
        self._wfReg                = webFactoryRegistry.WebFactoryRegistry()
        et                         = self._confData.get("event_type", "conference").strip()
        self._globalErrors         = []
        self._confData['modifKey'] = self._generatePassword(8)
        
        if et != "" and et !="default":
            self._wf = self._wfReg.getFactoryById( et )
    
    def _process( self ):
        
        ##@TODO Quitar los raise y cambiarlos por return false y desplegar el error
        # Colocar el api key.
        # Generar la contraseña de modificacion
        isNotValidRequest = self.checkRequest()
        if isNotValidRequest:
            return self._generateXMLError(isNotValidRequest)
            
        try:
            data = xmltodict.parse(unescape(self._params['xmlRequest']), encoding='utf-8')
        except:
            return self._generateXMLError("<msg>XML declaration not well-formed</msg>")
        
        validationResponse = self._checkForRequiredParams(data.get('data'))
        if validationResponse:
            return validationResponse        
        self._confData = self._convertDictToUTF(data.get('data'))
        self.initialize()
        validationResponse = self._validateDataTypes( self._confData )
        if validationResponse:
            return validationResponse
        
        
        
        #raise Exception(pprint.pformat( self._convertDictToUTF(self._confData )))
        self.initialize()
        self._user = self.registerUser( self._confData )
        c          = self._createEvent( self._confData )
        c.setModifKey(self._confData['modifKey'])
        self.alertCreation([c])
        self._confData['id'] = c.getId();
        self._confData['categoryTitle'] = c.getOwner().getTitle()
        self._confData['password'] = 'mysecretpassword'
        self._confData['url'] = urlHandlers.UHConferenceDisplay.getURL(c)
        
        return self._xmlSuccessResponse(self._confData)
    
    def checkRequest(self):
        if "api_key" in self._params:
            if not self._params["api_key"] == "693b2d384d2c395331243253662b5f5d6c594d2c733b7227333f79505c":
                return "<msg>Invalid api_key</msg>"
        else:
            return "<msg>The api_key is required</msg>"
        
        if not "xmlRequest" in self._params:
            return "<msg>The xmlRequest param is required</msg>"
        
        return False
            
    
    def registerUser(self, params):
        ih    = AuthenticatorMgr()
        minfo = info.HelperMaKaCInfo.getMaKaCInfoInstance()
        ah    = user.AvatarHolder()
        res   =  ah.match({"email": params["email"]}, exact=1, forceWithoutExtAuth=True)
        
        if res:
            #we find a user with the same email            
            a = res[0]
            return a
        else:
            a = user.Avatar()
            self.setUserData( a, params )
            a.activateAccount()
            ah.add(a)
            DBMgr.getInstance().commit()
            mail.sendConfirmationRequest(a).send()
            if minfo.getNotifyAccountCreation():
                mail.sendAccountCreationNotification(a).send()
            return a
        
        return False
    
    def setUserData(self, a, userData, reindex=False):
        a.setName( userData["name"], reindex=reindex )
        a.setSurName( userData["surName"], reindex=reindex )
        a.setOrganisation( "RedClara", reindex=reindex )
        if userData.has_key("lang"):
            a.setLang( userData["lang"] )
        a.setAddress( "RedClara" )
        a.setEmail( userData["email"], reindex )
        
    def _createEvent(self, params):
        c = self._target.newConference( self._user )
        self.setConferenceValues(c, params)

        if self._wf:
            self._wfReg.registerFactory( c, self._wf )

        eventAccessProtection = params.get("eventProtection", "inherit")

        if eventAccessProtection == "private" :
            c.getAccessController().setProtection(1)
        elif eventAccessProtection == "public" :
            c.getAccessController().setProtection(-1)
            
        #c.addChair(self._user)
        c.grantModification(self._user)
        c.getAccessController().grantSubmission(self._user)
        #avatars, newUsers, allowedAvatars = self._getPersons()
        #UtilPersons.addToConf(avatars, newUsers, allowedAvatars, c, self._params.has_key('grant-manager'), self._params.has_key('presenter-grant-submission'))
        if params.get("sessionSlots",None) is not None :
            if params["sessionSlots"] == "enabled" :
                c.enableSessionSlots()
            else :
                c.disableSessionSlots()
                
        return c
    
    def setConferenceValues(self, c, confData, notify=False):
        from MaKaC.webinterface.common.tools import escape_tags_short_url
        c.setTitle( confData["title"] )
        c.setDescription( confData["description"] )
        c.setOrgText(confData.get("orgText",""))
        c.setComments(confData.get("comments",""))
        c.setKeywords( confData.get("keywords", "") )
        c.setChairmanText( confData.get("chairText", "") )
        if "shortURLTag" in confData.keys():
            tag = confData["shortURLTag"].strip()
            tag = escape_tags_short_url(tag)
            if c.getUrlTag() != tag:
                from MaKaC.common.url import ShortURLMapper
                sum = ShortURLMapper()
                sum.remove(c)
                c.setUrlTag(tag)
                if tag:
                    sum.add(tag, c)
        c.setContactInfo( confData.get("contactInfo","") )
        #################################
        # Fermi timezone awareness      #
        #################################
        c.setTimezone(confData.get("Timezone", "UTC"))
        tz = confData.get("Timezone", "UTC")
        try:
            sDate = timezone(tz).localize(datetime(int(confData["sYear"]), \
                                 int(confData["sMonth"]), \
                                 int(confData["sDay"]), \
                                 int(confData["sHour"]), \
                                 int(confData[ "sMinute"])))
        except ValueError,e:
            raise FormValuesError("The start date you have entered is not correct: %s"%e, "Event")

        if confData.get("duration","") != "":
            eDate = sDate + timedelta(minutes=confData["duration"])
        else:
            try:
                eDate = timezone(tz).localize(datetime(   int(confData["eYear"]), \
                                     int(confData["eMonth"]), \
                                     int(confData["eDay"]), \
                                     int(confData["eHour"]), \
                                     int(confData[ "eMinute"])))
            except ValueError,e:
                raise FormValuesError("The end date you have entered is not correct: %s"%e)
        moveEntries = int(confData.get("move",0))
        c.setDates( sDate.astimezone(timezone('UTC')), \
                    eDate.astimezone(timezone('UTC')), moveEntries=moveEntries)

        #################################
        # Fermi timezone awareness(end) #
        #################################
        changed = False
        newLocation = confData.get("locationName","")
        newRoom = confData.get( "locationBookedRoom" )  or  \
                   confData.get( "roomName" )  or  ""

        if newLocation.strip() == "":
            c.setLocation( None )
        else:
            l = c.getLocation()
            if not l:
                l = conference.CustomLocation()
                c.setLocation( l )

            if l.getName() != newLocation:
                l.setName(newLocation)
                changed = True

            l.setAddress( confData.get("locationAddress","") )

        if newRoom.strip() == "":
            c.setRoom( None )
        else:
            r = c.getRoom()
            if not r:
                r = conference.CustomRoom()
                c.setRoom( r )

            if r.getName() != newRoom:
                r.setName(newRoom)
                r.retrieveFullName(newLocation)
                changed = True

        if changed:
            c._notify('placeChanged')

        emailstr = setValidEmailSeparators(confData.get("supportEmail", ""))

        if (emailstr != "") and not validMail(emailstr):
            #@TODO Retornar el error adecuado
            raise FormValuesError("One of the emails specified or one of the separators is invalid")

        c.getSupportInfo().setEmail(emailstr)
        c.getSupportInfo().setCaption(confData.get("supportCaption","Support"))
        displayMgr.ConfDisplayMgrRegistery().getDisplayMgr(c).setDefaultStyle(confData.get("defaultStyle",""))
        if c.getVisibility() != confData.get("visibility",999):
            c.setVisibility( confData.get("visibility",999) )
        curType = c.getType()
        newType = confData.get("eventType","")
        if newType != "" and newType != curType:
            wr = webFactoryRegistry.WebFactoryRegistry()
            factory = wr.getFactoryById(newType)
            wr.registerFactory(c,factory)
            dispMgr = displayMgr.ConfDisplayMgrRegistery().getDisplayMgr(c)
            styleMgr = info.HelperMaKaCInfo.getMaKaCInfoInstance().getStyleManager()
            dispMgr.setDefaultStyle(styleMgr.getDefaultStyleForEventType(newType))
            
    def _convertDictToUTF(self, data):
        output = {}
        
        for key, value in data.items():
            output[key.encode('utf-8')] = value.encode('utf-8')
            
        return output
        
        
    def _checkForRequiredParams(self, params):
        required = ['email', 'name', 'surName', 'categId', 'title', 'sDay', 'sMonth', 'sYear', 'sHour', 'sMinute', 'eDay', 'eMonth', 'eYear', 'eHour', 'eMinute', 'description']
        errors = []
        for require in required:
            if not require in params:
                errors.append(require)
                
        if len(errors) == 0:
            return False
        
        return """
        <?xml version="1.0" encoding="UTF-8"?>
            <data>
                <cmdStatus>FAILED</cmdStatus>
                <msg>The next attributes are required: %s</msg>
            </data>
        """%(', ' . join([str(error) for error in errors]))
    
    def _validateDataTypes(self, params):
        
        if not self._validMail(params['email']):
            return self._generateXMLError("""<msg>You must enter a valid email address: %s</msg>"""%params['email'])
        
        errors = self._validateDates(params)
        if errors:
            return self._generateXMLError(errors)
        
        if self._target is None:
            return self._generateXMLError("""<msg>Category with id %s not found</msg>"""%params['categId'])
        
        return False
    
    def _generateXMLError(self, tag):
        return """
        <?xml version="1.0" encoding="UTF-8"?>
            <data>
                <cmdStatus>FAILED</cmdStatus>
                %s
            </data>
        """%tag
        
    def _validMail(self,email):
        if re.search("^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$",email):
            return True
        return False
            
    def _validateDates(self, params):
        
        tz = params.get("Timezone", "UTC")
        try:
            eDate = timezone(tz).localize(datetime(   int(params["eYear"]), \
                                 int(params["eMonth"]), \
                                 int(params["eDay"]), \
                                 int(params["eHour"]), \
                                 int(params[ "eMinute"])))
        except ValueError,e:
            return  """<msg>The End Date is invalid: %s</msg>"""%e
        
        try:
            sDate = timezone(tz).localize(datetime(   int(params["sYear"]), \
                                 int(params["sMonth"]), \
                                 int(params["sDay"]), \
                                 int(params["sHour"]), \
                                 int(params[ "sMinute"])))
        except ValueError,e:
            return  """<msg>The next attributes are required: %s</msg>"""%e
        
        return False
            
    def _xmlSuccessResponse(self, conf):
        xml = """
        <?xml version="1.0" encoding="utf-8"?>
            <data>
                <cmdStatus>SUCCESS</cmdStatus>
        """
        
        for key, value in conf.items():            
            xml += """<%s>%s</%s>"""%(key, value, key)
            
        xml += """
        </data>
        """
        return xml
        
    def alertCreation(self, confs):
        conf = confs[0]
        fromAddr = Config.getInstance().getSupportEmail()
        addrs = [ Config.getInstance().getSupportEmail() ]
        eventType = conf.getType()
        if eventType == "conference":
            type = "conference"
        elif eventType == "meeting":
            type = "meeting"
        else:
            type = "lecture"
        chair = ""
        if conf.getChairmanText() != "":
            chair = conf.getChairmanText()
        else:
            for c in conf.getChairList():
                chair += c.getFullName() + "; "
        subject = "New %s in indico (%s)" % (type,conf.getId())
        if conf.getRoom() != None:
            room = conf.getRoom().getName()
        else:
            room = ""
        categoryTitle = conf.getOwner().getTitle()
        text = """
_Category_
%s
_Title_
%s
_Speaker/Chair_
%s
_Room_
%s
_Description_
%s
_Creator_
%s (%s)"""%(categoryTitle, conf.getTitle(), chair, room, conf.getDescription(), conf.getCreator().getFullName(), conf.getCreator().getId())
        if len(confs) == 1:
            text += """
_Date_
%s -> %s
_Access_
%s""" % ( conf.getAdjustedStartDate(), conf.getAdjustedEndDate(), urlHandlers.UHConferenceDisplay.getURL(conf))
        else:
            i = 1
            for c in confs:
                text += """
_Date%s_
%s -> %s
_Access%s_
%s """ % (i,c.getAdjustedStartDate(), c.getAdjustedEndDate(), i,urlHandlers.UHConferenceDisplay.getURL(c))
                i+=1

        msg = ("Content-Type: text/plain; charset=\"utf-8\"\r\nFrom: %s\r\nReturn-Path: %s\r\nTo: %s\r\nCc: \r\nSubject: %s\r\n\r\n"%(fromAddr, fromAddr, addrs, subject))
        msg = msg + text
        maildata = { "fromAddr": fromAddr, "toList": addrs, "subject": subject, "body": text }
        GenericMailer.send(maildata)
        
        # Category notification
        if conf.getOwner().getNotifyCreationList() != "":
            addrs2 = [ conf.getOwner().getNotifyCreationList() ]
            maildata2 = { "fromAddr": fromAddr, "toList": addrs2, "subject": subject, "body": text }
            GenericMailer.send(maildata2)
            
    def _generatePassword(self, length=8):
        alphabet = "abcdefghijklmnopqrstuvwxyz"
        pw_length = length
        mypw = ""

        for i in range(pw_length):
            next_index = random.randrange(len(alphabet))
            mypw = mypw + alphabet[next_index]

        # replace 1 or 2 characters with a number
        for i in range(random.randrange(1,3)):
            replace_index = random.randrange(len(mypw)//2)
            mypw = mypw[0:replace_index] + str(random.randrange(10)) + mypw[replace_index+1:]

        # replace 1 or 2 letters with an uppercase letter
        for i in range(random.randrange(1,3)):
            replace_index = random.randrange(len(mypw)//2,len(mypw))
            mypw = mypw[0:replace_index] + mypw[replace_index].upper() + mypw[replace_index+1:]
            
        return mypw
